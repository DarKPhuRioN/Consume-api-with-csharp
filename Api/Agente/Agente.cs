﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.APi;

/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.Agente
{
    public class Agente
    {
        public void neuralNetwork(Board board, Json api)
        {
            // board.getTurn() => Devuelve el turno
            // board.getPlayers() => Devuelve un arreglo de cada casilla
            // board.getAllDataModel() => toString Opcional del tablero
            // api.UpdateMovement(int movement) Movimiento 0 Quieto, 1 Adelante 2 Arriba 3 Abajo
            Console.Write(board.getTurn());

            api.UpdateMovement(1);

        }

        public void gameOver()
        {
            Console.Write("Game Over");
        }

        public void winner()
        {
            Console.Write("Ganaste");
        }

        public void dead()
        {
            Console.Write("Muerto");
        }
    }
}

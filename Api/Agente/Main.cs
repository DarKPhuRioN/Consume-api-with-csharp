﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.APi;
/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.Agente
{
    public class Program
    {
        static void Main(string[] args)
        {
            int code_public = 1, code_private = 123;
            bool dev = false;

            Json api = new Json("http://97fb4890.ngrok.io", code_private, code_public, dev);
            api.UpdateMovement(3);

            Agente ag = new Agente();
            api.recursive(ag,1);
            Console.ReadKey();
        }
    }
}
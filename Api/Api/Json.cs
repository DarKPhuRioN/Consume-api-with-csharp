﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Api.Agente;
using Newtonsoft.Json;

/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.APi
{
    public class Json
    {
        private Consume_api app;
        private int codePrivate, codePublic;
        private Test test;
        private bool dev;

        public Json(string url,int codePrivate, int codePublic, bool dev)
        {
            this.app = new Consume_api(url);
            this.codePrivate = codePrivate;
            this.codePublic = codePublic;
            this.dev = dev;
            this.test = new Test(this.codePublic);
        }

        public Board getData()
        {
            string json1 = app.getListBoxs(this.codePublic);
            Board bb = JsonConvert.DeserializeObject<Board>((json1));
            return bb;
        }

        public bool UpdateMovement(int movement)
        {
            return (dev) ? app.sendPostRequest(this.codePrivate, movement) : true;
        }

        public void recursive(Agente.Agente ag, int i)
        {
            Board table = (dev) ? this.getData() : this.test.getData(i);
            switch (table.getTurn())
            {
                default:
                    ag.neuralNetwork(table, this);
                    recursive(ag, i+1);
                    break;
                case -1:
                    ag.gameOver();
                    break;
                case -3:
                    ag.winner();
                    break;
                case -10:
                    ag.dead();
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.APi
{
    public class Consume_api
    {
        private string url = "";
        public Consume_api(string url)
        {
            this.url = url;
        }

        public bool sendPostRequest(int code, int movement)
        {
            try
            {
                string postData = "code=" + code + "&movimiento=" + movement, resp = "";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url + "/user/update-movement");
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded"; //"application/json; charset=utf-8";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                req.ContentLength = byteArray.Length;
                using (Stream dataStream = req.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(dataStream))
                    {
                        stmw.Write(postData);
                    }
                }
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                using (StreamReader reader = new StreamReader(res.GetResponseStream()))
                {
                    resp = reader.ReadToEnd();
                }
                Console.Write(resp.ToString()+"\n");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        public string getListBoxs(int codePublic)
        {
            try
            {
                System.Net.WebRequest wr = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + "/box/table/get-table/"+ codePublic);
                wr.Method = "GET";
                wr.ContentType = "application/x-www-form-urlencoded";
                System.IO.Stream sb;
                System.Net.WebResponse response = wr.GetResponse(); // Obtiene la respuesta
                sb = response.GetResponseStream(); // Stream con el contenido recibido del servidor
                System.IO.StreamReader reader = new System.IO.StreamReader(sb);
                // Cerramos los streams
                string resp = reader.ReadToEnd();
                reader.Close();
                sb.Close();
                response.Close();
                return resp;
            }
            catch (Exception ex)
            {
                return "Error :" + ex.ToString();
            }
        }
    }
}
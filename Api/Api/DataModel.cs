﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.APi
{
    public class DataModel
    {
        [JsonProperty("casilla")]
        private string box { get; set; }
  
        [JsonProperty("turno")]
        private string color { get; set; }
        
        [JsonProperty("usuario")]
        private int user { get; set; }

         public DataModel(String box, String color, int user)
         {
             this.box = box;
             this.color = color;
             this.user = user;
         }
         
        public string getBox()
        {
            return box;
        }

        public string getColor() {
            return color;
        }
        
        public int getUser()
        {
            return user;
        }

        public void setBox(string box)
        {
            this.box = box;
        }

        public string getDataModels()
        {
            return "Casilla : " + box + " Usuario : " + user + " Color : "+ color;
        }
    }
}

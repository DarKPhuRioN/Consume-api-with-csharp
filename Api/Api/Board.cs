﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

/**
 * @author Julio Alfredo Vásquez Lievano
 */

namespace Api.APi
{
    public class Board
    {
        [JsonProperty("turno")]
        private int turn { get; set; }
        [JsonProperty("tablero")]
        private DataModel[] Boards { get; set; }

        public Board(int turn, DataModel[] Boards)
        {
            this.turn = turn;
            this.Boards = Boards;
        }
        public int getTurn()
        {
            return turn;
        }

        public string getAllDataModel()
        {
            string datamodel = " => Turno : " + turn + " <= \n";
            if (Boards != null)
            {
                for (int i = 0; i < Boards.Length; i++)
                {
                    datamodel += Boards[i].getDataModels() + "\n";
                }
                return datamodel;
            }
            return "";   
        }

        public DataModel[] getPlayers()
        {
            return this.Boards;
        }
        public void setTurn(int turn)
        {
            this.turn = turn;
        }

        public void setDataModel(DataModel[] datamodel)
        {
            this.Boards = datamodel;
        }
    }
}
